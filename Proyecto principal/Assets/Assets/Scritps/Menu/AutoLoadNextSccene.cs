using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoLoadNextSccene : MonoBehaviour
{
    public float delayBeforeLoading = 2f;
    public string nextSceneName = "Jp";

    private float timer = 0f;

    private void Update()
    {
        timer += Time.deltaTime;

        if( timer >= delayBeforeLoading)
        {
            LoadNextScene();
        }
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(nextSceneName);
    }
}
