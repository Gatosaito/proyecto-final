using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioPantalla : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void EscenaJuego()
    {
        SceneManager.LoadScene("Jp");
    }

    public void Opciones()
    {
        SceneManager.LoadScene("Opciones");
    }
    public void Loading()
    {
        SceneManager.LoadScene("LoadingScreen");
    }

    public void Instrucciones()
    {
        SceneManager.LoadScene("Instrucciones");    }

    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void Salir()
    {
        Application.Quit();
    }


}
