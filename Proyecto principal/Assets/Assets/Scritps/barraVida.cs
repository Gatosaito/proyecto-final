using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class barraVida : MonoBehaviour
{
    public Slider sliderVida;

    private void Start()
    {
        sliderVida = GetComponent<Slider>();
    }

    public void CambiarVidaMaxima(float vidaMaxima)
    {
        sliderVida.maxValue = vidaMaxima;
    }

    public void CambiarVidaActual(float cantidadDeVide) 
    {
        sliderVida.value = cantidadDeVide;
    }

    public void InicializarBarraDeVida(float cantidadDeVide)
    {
        CambiarVidaMaxima(cantidadDeVide);
        CambiarVidaActual(cantidadDeVide);
    }

    public void ActualizarBarraDeVida(float cantidadDeVida)
    {
        Debug.Log("Actualizando barra de vida a: " + cantidadDeVida);
        sliderVida.value = cantidadDeVida;
    }
}
