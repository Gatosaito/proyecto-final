using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Muerte_Boss : MonoBehaviour
{
    public float Health;
    public string scene;
    // Update is called once per frame
    void Update()
    {
        MuerteBoss();
    }

    public void MuerteBoss()
    {
        Health = GetComponent<Enemys>().vida;

        if (Health <= 0)
        {
            SceneManager.LoadScene(scene);
            Destroy(gameObject);
        }
    }

}
