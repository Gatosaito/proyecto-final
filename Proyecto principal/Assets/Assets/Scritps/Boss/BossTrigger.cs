using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{

    public GameObject bossGO;
    public GameObject Music_Base;
  

    private void Start()
    {
        bossGO.SetActive(false);
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            SceneBoss.instance.ActivateBoss();
            Debug.Log("Entro");
            StartCoroutine(WaitForBoss());
            Music_Base.SetActive(false);

             
        }
    }

    IEnumerator WaitForBoss()
    {
        var currentSpeed = PlayeMovenmetn.instance.runSpeed;
        PlayeMovenmetn.instance.runSpeed = 0;
        bossGO.SetActive(true);
        yield return new WaitForSeconds(3f);
        PlayeMovenmetn.instance.runSpeed = currentSpeed;
        Destroy(gameObject);
    }
}
