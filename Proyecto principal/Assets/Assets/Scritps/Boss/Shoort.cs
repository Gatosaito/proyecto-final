using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamer : MonoBehaviour
{
    float moveSpeed;
    Rigidbody2D rb2d;
    Vector2 moveDirection;
    PlayeMovenmetn target;

    private void Update()
    {
        moveSpeed = GetComponent<Enemy_patroll>().moveSpeed;
        rb2d = GetComponent<Rigidbody2D>();
        target = PlayeMovenmetn.instance;

        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb2d.velocity = new Vector2(moveDirection.x, moveDirection.y);
    }

 
}
