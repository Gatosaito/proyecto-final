using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneBoss : MonoBehaviour
{
    public GameObject bossPanel;
    public GameObject piedra;


    public static SceneBoss instance;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        bossPanel.SetActive(false);
        piedra.SetActive(false);
    }

    public void ActivateBoss()
    {
        bossPanel.SetActive(true); 
        piedra.SetActive(true);
    }


}
