using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossAtras : MonoBehaviour
{
    public Transform[] transforms;
    public GameObject flame;

    public float timeShoot,countdown;
    public float timeTp, countdownTP;
    public float currentHealth, bossHealth;

    public Image healdIm;
    private void Start()
    {
        var initialPosition = Random.Range(0, transforms.Length);

        transform.position = transforms[initialPosition].position;
        countdown = timeShoot;
        countdownTP = timeTp;
    }

    private void Update()
    {
       Countdown();
       DamageBoss();
       Destroy(flame, 3f);
    }

    public void Countdown()
    {
        countdown -= Time.deltaTime;
        countdownTP -= Time.deltaTime;

        if (countdown < 0)
        {
            ShootPlayer();
            countdown = timeShoot;
        }

        if (countdownTP <= 0)
        {
            countdownTP = timeTp;
            Teleport();
        }

    }

    public void ShootPlayer()
    {
        GameObject spell = Instantiate(flame, transform.position, Quaternion.identity);
    }

    public void Teleport()
    {
        var initialPosition = Random.Range(0, transforms.Length);
        transform.position = transforms[initialPosition].position;
    }
            
    public void DamageBoss()
    {
        currentHealth = GetComponent<Enemy_patroll>().Vida;
        healdIm.fillAmount = currentHealth / bossHealth;
    }
}
