using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxEfect : MonoBehaviour
{
    [SerializeField] private float paralaxMultiplier;
    [SerializeField] private Transform cameraTransform;
    private Vector3 previousCameraPosition;
    private float spriteWidth, startPosition, initialYPosition;

    void Start()
    {
        cameraTransform = Camera.main.transform;
        previousCameraPosition = cameraTransform.position;
        spriteWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        startPosition = transform.position.x;
        initialYPosition = transform.position.y; // Guarda la posici�n inicial en Y
    }

    void LateUpdate()
    {
        float deltaX = (cameraTransform.position.x - previousCameraPosition.x) * paralaxMultiplier;
        float moveAmountX = cameraTransform.position.x * (1 - paralaxMultiplier);

        // No muevas el objeto en el eje Y
        float newYPosition = transform.position.y;

        transform.Translate(new Vector3(deltaX, 0, 0));
        transform.position = new Vector3(transform.position.x, newYPosition, transform.position.z);

        previousCameraPosition = cameraTransform.position;

        if (moveAmountX > startPosition + spriteWidth)
        {
            transform.Translate(new Vector3(spriteWidth, 0, 0));
            startPosition += spriteWidth;
        }
        else if (moveAmountX < startPosition - spriteWidth)
        {
            transform.Translate(new Vector3(-spriteWidth, 0, 0));
            startPosition -= spriteWidth;
        }
    }
}

