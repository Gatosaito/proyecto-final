using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida_tarrito : MonoBehaviour
{
    public PlayeMovenmetn Player;
    public barraVida barra;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Vida") && Player.vida < 100)
        {
            Player.vida += 50;
            barra.CambiarVidaActual(Player.vida);
            Destroy(gameObject, 0f);
            Debug.Log("Hola");
        }

        if(Player.vida > 100)
        {
            Player.vida = 100;
        }

    }
}
