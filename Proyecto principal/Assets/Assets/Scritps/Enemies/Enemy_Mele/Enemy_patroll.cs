using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_patroll : MonoBehaviour
{
    public float Vida;
    public bool morir;
    public Transform pointA;
    public Transform pointB;
    public float moveSpeed = 3f;
    public float attackRange = 1.5f;
    public int attackDamage = 10;
    public float detectionRange = 5f;

    private Transform player;
    private Vector2 targetPosition;
    private SpriteRenderer rendererX;

    public Animator animator;


    //public AudioClip runSound;
    public AudioClip attackSound;
    public AudioClip deathSound;

    public static Enemy_patroll instance;

    public bool canAttack = true;
    public float attackCooldown = 2.0f;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        targetPosition = pointA.position;
        rendererX = GetComponent<SpriteRenderer>();
        animator.SetBool("die", false);

    }

    void Update()
    {
        Morir();

        if (!morir) // Verifica si el enemigo no ha muerto
        {
            if (player != null)
            {
                float distanceToPlayer = Vector2.Distance(transform.position, player.position);

                if (distanceToPlayer <= attackRange)
                {
                    // Si el jugador est� en rango de ataque, ataca
                    Attack();
                }
                else if (distanceToPlayer <= detectionRange)
                {
                    // Si el jugador est� en rango de detecci�n, persigue al jugador
                    targetPosition = player.position;
                    animator.SetBool("atack", false);
                    animator.SetBool("run", true);

                    if (transform.position.x < player.transform.position.x)
                    {
                        rendererX.flipX = false;
                    }
                    else
                    {
                        rendererX.flipX = true;
                    }

                    // Mant�n la posici�n en Y del enemigo constante
                    targetPosition.y = transform.position.y;
                }
            }

            Patrol();
        }
        else
        {
            // El enemigo ha muerto, puedes agregar l�gica adicional aqu� si es necesario
        }
    }

    void Patrol()
    {
        if (!morir) // Verifica si el enemigo no ha muerto
        {
            animator.SetBool("die", false);

            Vector2 direction = (targetPosition - (Vector2)transform.position).normalized;

          

            transform.Translate(direction * moveSpeed * Time.deltaTime);


            if (Vector2.Distance(transform.position, targetPosition) < 0.1f)
            {
                if (targetPosition == (Vector2)pointA.position)
                {
                    targetPosition = pointB.position;
                    rendererX.flipX = false;
                }
                else
                {
                    targetPosition = pointA.position;
                    rendererX.flipX = true;
                }
            }
           // AudioSource.PlayClipAtPoint(runSound, gameObject.transform.position);

        }
    }

    void Attack()
    {
        if (!morir) 
        {
            if (canAttack)
            {
                canAttack = false;

                AudioSource.PlayClipAtPoint(attackSound, gameObject.transform.position);
                Invoke("ResetAttackCooldown", attackCooldown);
                animator.SetBool("atack", true);
                animator.SetBool("run", false);
            }
        }
    }

    void ResetAttackCooldown()
    {
        animator.SetBool("atack", false);
        animator.SetBool("run", false);
        canAttack = true;
    }

    public void Morir()
    {
        if (Vida <= 0 && !morir) 
        {
            animator.SetBool("atack", false);
            animator.SetBool("run", false);
            animator.SetBool("die", true);
            morir = true;

            if (morir == true)
            {

                AudioSource.PlayClipAtPoint(deathSound, gameObject.transform.position);

                Destroy(gameObject, 3f);
            }
        }
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Sword") && Vida > 0)
        {
            Vida-=1;
        }
    }
}

