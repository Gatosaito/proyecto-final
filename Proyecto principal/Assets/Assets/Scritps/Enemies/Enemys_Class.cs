using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Melee,
    Tank,
    Distance
}
[CreateAssetMenu(fileName = "NewEnemyData", menuName = "ScriptableObjects/EnemyData")]
public class Enemys_Class : ScriptableObject
{
    public EnemyType enemyType;
    public float maxHealth;
    public float damage;
    public float speed;
}
