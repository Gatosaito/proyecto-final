using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemys: MonoBehaviour
{
    [SerializeField] public float vida;
    public static Enemys instante;

    private void Awake()
    {
        if (instante == null)
        {
            instante = this;
        }
    }
    public void TomarDa�o(float da�o)
    {
        vida -= da�o;
        if (vida <= 0)
        {
            Debug.Log("Morido");
        }
    }
}
