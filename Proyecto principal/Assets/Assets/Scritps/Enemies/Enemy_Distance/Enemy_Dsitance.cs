using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDistance : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float speed;
    [SerializeField] private float waitTime;
    [SerializeField] private Transform[] waypoints;
    private int currentWaypoint;
    private bool isWaiting;

    [Header("Shooting")]
    [SerializeField] private GameObject flechaInstance;
    [SerializeField] private float flechaSpeed;
    private bool isShooting = false;
    private bool canShoot = true;
    private float attackDuration = 1.5f;

    [Header("Detection")]
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float detectionRange = 9f;
    private bool detect = false;

    [Header("UI and Animation")]
    public Animator enemyDistance;
    public SpriteRenderer arc;
    private float previousXPosition;

    private void Start()
    {
        currentWaypoint = Random.Range(0, waypoints.Length);
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        previousXPosition = transform.position.x;
    }

    private void Update()
    {
        float distanceToPlayer = Vector2.Distance(transform.position, playerTransform.position);

        if (distanceToPlayer <= detectionRange)
        {
            detect = true;
        }
        else
        {
            detect = false;
            Patrol();
            StopCoroutine(ShootProjectile());
        }

        float currentXPosition = transform.position.x;

        if (currentXPosition < previousXPosition)
        {
            arc.flipX = true;
        }
        else if (currentXPosition > previousXPosition)
        {
            arc.flipX = false;
        }

        previousXPosition = currentXPosition;

        if (detect)
        {
            if (distanceToPlayer <= 9f && !isShooting)
            {
                isShooting = true;
                StartCoroutine(ShootProjectile());
            }
        }
    }

    private void Patrol()
    {
        if (!detect)
        {
            if (transform.position != waypoints[currentWaypoint].position)
            {
                transform.position = Vector2.MoveTowards(transform.position, waypoints[currentWaypoint].position, speed * Time.deltaTime);
                enemyDistance.SetBool("run", true);
            }
            else if (!isWaiting)
            {
                StartCoroutine(Wait());
            }
        }
        else
        {
            enemyDistance.SetBool("idle", true);
            enemyDistance.SetBool("run", false);
        }
    }

    private void ChasePlayer()
    {
        if (detect && canShoot)
        {
            canShoot = false;
            isShooting = true;
            enemyDistance.SetBool("atack", true);
            enemyDistance.SetBool("run", false);

            StartCoroutine(ShootProjectile());
        }
    }

    private IEnumerator ShootProjectile()
    {
        while (true)
        {
            Vector2 directionToPlayer = (playerTransform.position - transform.position).normalized;

            GameObject projectile = Instantiate(flechaInstance, transform.position, Quaternion.identity);
            Rigidbody2D rbProjectile = projectile.GetComponent<Rigidbody2D>();
            rbProjectile.velocity = directionToPlayer * speed;
            Destroy(projectile, 4f);
            enemyDistance.SetBool("atack", true);

            yield return new WaitForSeconds(attackDuration);
        }
    }

    private IEnumerator Wait()
    {
        isWaiting = true;
        enemyDistance.SetBool("run", false);
        enemyDistance.SetBool("idle", true);
        enemyDistance.SetBool("atack", false);

        yield return new WaitForSeconds(waitTime);
        currentWaypoint++;
        if (currentWaypoint == waypoints.Length)
        {
            currentWaypoint = 0;
        }
        isWaiting = false;
        enemyDistance.SetBool("idle", false);
    }
}
