using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DMG_flecha : MonoBehaviour
{
   [SerializeField] public bool pega;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            pega = true;
            Debug.Log("Proyectil golpe� al jugador");
        }

        else
        {
            pega = false;
        }
    }
}