using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Tank : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float waitTime;
    [SerializeField] private Transform[] waypoints;

    private int currentWaypoint;
    private bool isWaiting;
    public bool detect;
    private Transform playerTransform;
    public float detectionRange = 5f;

    private void Start()
    {
        currentWaypoint = Random.Range(0, waypoints.Length);
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        float distanceToPlayer = Vector2.Distance(transform.position, playerTransform.position);

        if (distanceToPlayer <= detectionRange)
        {
            detect = true;
        }
        else
        {
            detect = false;
            Patrol();
        }

        if (detect == true)
        {
            if (distanceToPlayer <= 3f)
            {
                Debug.Log("Melee Attack");
            }
            ChasePlayer();
        }
    }

    private void Patrol()
    {
        if (!detect)
        {
            if (transform.position != waypoints[currentWaypoint].position)
            {
                transform.position = Vector2.MoveTowards(transform.position, waypoints[currentWaypoint].position, speed * Time.deltaTime);
            }
            else if (!isWaiting)
            {
                StartCoroutine(Wait());
            }
        }
    }

    private void ChasePlayer()
    {
        if (detect)
        {
            transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, speed * Time.deltaTime);

            if (Vector2.Distance(transform.position, playerTransform.position) > detectionRange)
            {
                detect = false;
            }
        }
    }

    private IEnumerator Wait()
    {
        isWaiting = true;
        yield return new WaitForSeconds(waitTime);
        currentWaypoint++;
        if (currentWaypoint == waypoints.Length)
        {
            currentWaypoint = 0;
        }
        isWaiting = false;
    }
}
