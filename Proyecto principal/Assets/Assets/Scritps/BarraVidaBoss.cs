using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BarraVidaBoss : MonoBehaviour
{

    public float Health, currentHealth;

    public Image healdIm;
    // Update is called once per frame
    void Update()
    {
        DamageBoss();
    }

    public void DamageBoss()
    {
        Health = GetComponent<Enemy_patroll>().Vida;
        currentHealth = GetComponent<Enemys>().vida;
        healdIm.fillAmount = currentHealth / Health;
    }
}
