using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aparecer_cosas_bpss : MonoBehaviour
{
    public GameObject Poscion_1;
    public GameObject Poscion_2;
    public AudioClip Campana;

    private void Start()
    {
        Invoke("Vida_1", 20f);
        Invoke("Vida_2", 40f);
    }
    public void Vida_1()
    {
        AudioSource.PlayClipAtPoint(Campana, gameObject.transform.position);
        Poscion_1.SetActive(true);
    }
    public void Vida_2()
    {
        AudioSource.PlayClipAtPoint(Campana, gameObject.transform.position);
        Poscion_2.SetActive(true);
    }
}
