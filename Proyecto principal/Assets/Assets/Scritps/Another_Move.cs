using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Another_Move : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 10f;

    public float vida;
    public float vidaMax;

    public float da�o;

    private bool isGrounded;
    private Rigidbody2D rb;

    public barraVida barraDeVida;
   // public DMG_flecha da�oProyectil;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        vida = vidaMax;
        barraDeVida.InicializarBarraDeVida(vida);
    }

    void Update()
    {
        // Movimiento horizontal
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector2 movement = new Vector2(moveHorizontal * moveSpeed, rb.velocity.y);
        rb.velocity = movement;

        // Salto
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Arrow"))
        {
            Debug.Log("Vida -");
            vida -= da�o;
            barraDeVida.CambiarVidaActual(vida);
        }

            
    }

}