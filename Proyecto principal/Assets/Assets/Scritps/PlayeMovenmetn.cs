using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayeMovenmetn : MonoBehaviour
{
    [Header("Life")]
    [SerializeField] public float vida;
    [SerializeField] private float vidaMax;
    [SerializeField] private float da�o;


    [Header("Move")]
    [SerializeField] public float runSpeed = 5f;
    private bool mirandoDerecha = true;

    [Header("Jump")]
    [SerializeField] private float jumpForce;
    [SerializeField] private Transform groundController;
    [SerializeField] private Vector2 boxDimension;
    [SerializeField] private LayerMask isGround;

    private bool isGrounded;
    private bool isJumping;

    [Header("Regulable Jump")]
    [Range(0, 1)][SerializeField] private float multiplierCanceledJump;
    [SerializeField] private float gravityMultiplier;

    private float escalaGravedad;
    private bool jumpBottomUp = true;

    [Header("Attack")]
    [SerializeField] private Transform AttackController;
    [SerializeField] private float AttackRadio;
    [SerializeField] private float attackDamage;
    [SerializeField] private float tiempoEntreAtaques;
    [SerializeField] private float tiempoSiguienteAtaque;

    [Header("Referencias")]
    private Rigidbody2D rb2D;
    private Animator animator;
    public barraVida barraDeVida;
    public Enemy_patroll Enemy;
    public Slider sliderStamina;

    [Header("Dash")]
    [SerializeField] private float dashVelocity;
    [SerializeField] private float dashTime;
    [SerializeField] private float stamine = 100f;
    [SerializeField] private TrailRenderer trailRenderer;

    private float initialGravity;
    private bool puedeDashear = true;
    private bool sePuedeMover = true;

    [Header("Sonidos")]
    [SerializeField] private AudioClip walk;
    [SerializeField] private AudioClip dash;
    [SerializeField] private AudioClip jump;
    [SerializeField] private AudioClip attack;

    [SerializeField] private AudioSource audioSource;

    public string scene;
    public static PlayeMovenmetn instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } 
    }

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        escalaGravedad = rb2D.gravityScale;
        initialGravity = rb2D.gravityScale;

        vida = vidaMax;
        barraDeVida.InicializarBarraDeVida(vida);

        animator.SetBool("IsGrounded", true);
    }

    private void Update()
    {
        if (tiempoSiguienteAtaque > 0)
        {
            tiempoSiguienteAtaque -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire1") && tiempoSiguienteAtaque <= 0)
        {
            Golpe();
            AudioSource.PlayClipAtPoint(attack, gameObject.transform.position);

            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }

        if (Input.GetButton("Jump"))
        {
            isJumping = true;
        }

        if (Input.GetButtonUp("Jump"))
        {
            BotonSaltoArriba();
        }

        isGrounded = Physics2D.OverlapBox(groundController.position, boxDimension, 0, isGround);
        animator.SetBool("IsGrounded", isGrounded);

        if (Input.GetKeyDown(KeyCode.LeftShift) && puedeDashear && stamine > 50f)
        {
            StartCoroutine(Dash());
        }
        if (stamine != 100)
        {
            stamine += 10 * Time.deltaTime;
        }
        if (stamine > 100)
        {
            stamine = 100f;
        }

        sliderStamina.value = stamine;

        if(vida <= 0)
        {
            animator.SetBool("Death", true);
            Invoke("Reinicar", 0f);

        }
    }

    private void FixedUpdate()
    {
        if (sePuedeMover)
        {
            Movimiento();
        }

        if (isJumping && jumpBottomUp && isGrounded)
        {
            Salto();
            AudioSource.PlayClipAtPoint(jump, gameObject.transform.position);
        }

        if (rb2D.velocity.y < 0 && !isGrounded)
        {
            rb2D.gravityScale = escalaGravedad * gravityMultiplier;
        }
        else
        {
            rb2D.gravityScale = escalaGravedad;
        }

        isJumping = false;
    }

    public void Movimiento()
    {
        float inputMove = Input.GetAxis("Horizontal");

        rb2D.velocity = new Vector2(inputMove * runSpeed, rb2D.velocity.y);


        Orientacion(inputMove);

        animator.SetBool("IsWalking", Mathf.Abs(inputMove) > 0.1f);

    }

    private IEnumerator Dash() 
    {
        sePuedeMover = false;
        puedeDashear = false;
        rb2D.gravityScale = 0;
        stamine -= 50;
        rb2D.velocity = new Vector2(dashVelocity * transform.localScale.x, 0);
        animator.SetTrigger("Dash");
        AudioSource.PlayClipAtPoint(dash, gameObject.transform.position);

        trailRenderer.emitting = true;

        yield return new WaitForSeconds(dashTime);

        sePuedeMover = true;
        puedeDashear = true;
        rb2D.gravityScale = initialGravity;
        trailRenderer.emitting = false;
    }

    public void Orientacion(float inputMove) 
    {
        if ((mirandoDerecha == true && inputMove < 0) || (mirandoDerecha == false && inputMove > 0))
        {
            mirandoDerecha = !mirandoDerecha;

            Vector3 newScale = transform.localScale;
            newScale.x *= -1;
            transform.localScale = newScale;
            //transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }

    public void Salto() 
    {
        rb2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
        isJumping = false;
        jumpBottomUp = false;
    }

    private void BotonSaltoArriba() 
    {
        if (rb2D.velocity.y > 0)
        {
            rb2D.AddForce(Vector2.down * rb2D.velocity.y * (1 - multiplierCanceledJump), ForceMode2D.Impulse);
        }

        jumpBottomUp = true;
        isJumping = false;
    }

    public void Golpe() 
    {
        animator.SetTrigger("Attack");

        Collider2D[] objetos = Physics2D.OverlapCircleAll(AttackController.position, AttackRadio);

        foreach (Collider2D colisionador in objetos)
        {
            if (colisionador.CompareTag("Enemy"))
            {
                colisionador.transform.GetComponent<Enemys>().TomarDa�o(attackDamage);
                
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("enemy_dmg"))
        {
            vida -= Enemy.attackDamage;
            barraDeVida.CambiarVidaActual(vida);
        }

        if (collision.gameObject.CompareTag("Pinchos"))
        {
            SceneManager.LoadScene("Jp");
        }

    

    }

    

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(AttackController.position, AttackRadio);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireCube(groundController.position, boxDimension);
    }

    public void Reinicar()
    {
        SceneManager.LoadScene(scene);
    }

    public void SonidoAndar() 
    {
        AudioSource.PlayClipAtPoint(walk, gameObject.transform.position);
    }
}

